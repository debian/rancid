Author: Roland Rosenfeld <roland@debian.org>
Description: Fix some temp file security vulnerabilities by using mktemp(1) or 
 moving the lockfiles or tempfiles to /var/lib/rancid, where they
 should be safe.
Bug-Debian: https://bugs.debian.org/496426
Forwarded: https://github.com/haussli/rancid/issues/18

--- a/bin/control_rancid.in
+++ b/bin/control_rancid.in
@@ -131,14 +131,15 @@ if [ $# -lt 1 ] ; then
 else
     GROUP=$1
 fi
-TMP=${TMPDIR:=/tmp}/rancid.$GROUP.$$
+MYTMPDIR=`mktemp -d -t`
+TMP=$MYTMPDIR/rancid.$GROUP.$$
+trap 'rm -fr $MYTMPDIR;' 1 2 15
 if [ -z "$ENVFILE" ] ; then
     ENVFILE="@sysconfdir@/rancid.conf"
 fi
 if [ -f "$ENVFILE" ] ; then
     . $ENVFILE
 fi
-trap 'rm -fr $TMP;' 1 2 15
 DIR=$BASEDIR/$GROUP
 
 # disable noclobber
@@ -316,7 +317,7 @@ cvs | svn )
 	echo "There were $RCSSYS conflicts during update."
 	echo ""
 	cat $TMP
-	rm -f $TMP
+	rm -fr $MYTMPDIR
 	exit 1
     fi
     ;;
@@ -341,7 +342,7 @@ fi
 # generate the list of all, up, & down routers
 cd $DIR
 trap 'rm -fr routers.db routers.all.new routers.down.new routers.up.new \
-	routers.mail routers.added routers.deleted $TMP;' 1 2 15
+	routers.mail routers.added routers.deleted $MYTMPDIR;' 1 2 15
 @PERLV@ -ne '{s/^\s*//; s/\s*$//; s/\s*;\s*/;/g; next if (/^(#|;|$)/);
     @F = split /\;/;
     $F[2] =~ s/\s*\$//;
@@ -472,7 +473,7 @@ if [ $? -ne 0 ] ; then
     echo "Error: could not rename routers.up.new" >&2
 fi
 rm -f routers.db
-trap 'rm -fr $TMP;' 1 2 15
+trap 'rm -fr $MYTMPDIR;' 1 2 15
 
 cd $DIR/configs
 # check for 'up' routers missing in RCS.  no idea how this happens to some folks
@@ -551,7 +552,7 @@ fi
 
 # if a device (-r) was specified, see if that device is in this group
 if [ "X$device" != "X" ] ; then
-    trap 'rm -fr $TMP $DIR/routers.single;' 1 2 15
+    trap 'rm -fr $MYTMPDIR $DIR/routers.single;' 1 2 15
     devlistfile="$DIR/routers.single"
     grep -i "^$device\;" routers.up > $devlistfile
     if [ $? -eq 1 ] ; then
@@ -636,7 +637,7 @@ done
 
 # This has been different for different machines...
 # Diff the directory and then checkin.
-trap 'rm -fr $TMP $TMP.diff $DIR/routers.single;' 1 2 15
+trap 'rm -fr $MYTMPDIR $DIR/routers.single;' 1 2 15
 cd $DIR
 if [ "X$DIFFSCRIPT" = "X" ]; then
     case $RCSSYS in
@@ -789,5 +790,5 @@ if [ -s $DIR/routers.failed ] ; then
 fi
 
 # Cleanup
-rm -f $TMP.diff $DIR/routers.single $DIR/routers.failed
+rm -fr $MYTMPDIR $DIR/routers.single $DIR/routers.failed
 trap '' 1 2 15
--- a/bin/rancid-run.in
+++ b/bin/rancid-run.in
@@ -51,7 +51,7 @@
 # Default ENVFILE, overrideable with -f flag.
 ENVFILE="@sysconfdir@/rancid.conf"
 
-TMPDIR=${TMPDIR:=/tmp}; export TMPDIR
+TMPDIR=/var/lib/rancid; export TMPDIR
 
 # control_rancid argv
 CR_ARGV=""; export CR_ARGV
--- a/bin/rivlogin.in
+++ b/bin/rivlogin.in
@@ -27,7 +27,7 @@ set config   0
 set timeoutdflt 10
 # Some CLIs having problems if we write too fast (Extreme, PIX, Cat)
 set send_human {.2 .1 .4 .2 1}
-set tempfile "/tmp/rivlogin.[exec date]"
+set tempfile "/var/lib/rancid/rivlogin.[exec date]"
 
 # cli command prompt
 set my_prompt    ">"
--- a/man/rancid-run.1
+++ b/man/rancid-run.1
@@ -46,8 +46,7 @@ group by
 The lock file will be named
 .IR .<group>.run.lock
 and will be located in
-.IR $TMPDIR
-(see below).
+.IR /var/lib/rancid .
 .PP
 A log file is produced under
 .IR $LOGDIR/logs
@@ -118,7 +117,7 @@ Search path for utilities.
 .\"
 .TP
 .B TMPDIR
-Directory to hold temporary and lock files.
+Directory to hold temporary files.
 .SH ERRORS
 If rancid fails to run or collect a device's configuration, the particular
 group's log file (mentioned above) should be consulted.  Any errors produced
--- a/share/getipacctg
+++ b/share/getipacctg
@@ -58,17 +58,19 @@
 #
 # Contributed to rancid by Steve Neighorn of SCN Reasearch.
 
-TMP="/tmp/ipacct.$$.prefixes"
-TMP2="/tmp/ipacct.$$.sorted"
-TMP3="/tmp/ipacct.$$.pl"
-
 if [ $# -eq 0 ] ; then
     echo "usage: getipacctg router_name [<number of lines off the top>] [<src/dest prefix filter> [...]]" >&2
     exit 1;
 fi
 
-trap 'rm -fr /tmp/ipacct.$$ $TMP $TMP2 $TMP3;' 1 2 15
-clogin -c 'show ip accounting' $1 > /tmp/ipacct.$$
+MYTMPDIR=`mktemp -t -d`
+TMP="$MYTMPDIR/ipacct.prefixes"
+TMP2="$MYTMPDIR/ipacct.sorted"
+TMP3="$MYTMPDIR/ipacct.pl"
+TMPIPACCT="$MYTMPDIR/ipacct"
+
+trap 'rm -fr $MYTMPDIR;' 1 2 15
+clogin -c 'show ip accounting' $1 > $TMPIPACCT
 
 if [ $? -ne 0 ] ; then
     echo "clogin failed." >&2
@@ -89,7 +91,7 @@ while [ $# -ne 0 ] ; do
 done
 6>&-
 
-egrep '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+ +[0-9]+\.[0-9]+\.' /tmp/ipacct.$$ | \
+egrep '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+ +[0-9]+\.[0-9]+\.' $TMPIPACCT | \
 	sed -e 's/^ *//' -e 's/  */ /g' -e 's/.$//' | \
 	awk '{print $4":"$0;}' | sort -nr | \
 	sed -e 's/^[^:]*://' > $TMP2
@@ -140,6 +142,6 @@ else
     $HEAD $TMP2
 fi
 
-rm -fr /tmp/ipacct.$$ $TMP $TMP2 $TMP3
+rm -fr $MYTMPDIR
 trap ';' 1 2 15
 exit 0
