rancid (3.13-4) unstable; urgency=medium

  * Update debian/copyright with support by lrc.
  * Get rid of alpha stage warning and "Really continue?" debconf question.
  * Update to Standards-Version 4.7.0 (no changes).

 -- Roland Rosenfeld <roland@debian.org>  Sat, 21 Dec 2024 16:59:24 +0100

rancid (3.13-3) unstable; urgency=medium

  * Update to Standards-Version 4.6.2 (no changes).
  * Preserve files deleted by distclean and repair clean target
    (Closes: #1046754).

 -- Roland Rosenfeld <roland@debian.org>  Mon, 21 Aug 2023 18:00:16 +0200

rancid (3.13-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + rancid: Drop versioned constraint on debconf in Depends.

  [ Roland Rosenfeld ]
  * Update to Standards-Version 4.6.1 (no changes).
  * debian/control: update Build-Depends for cross builds.
  * 31_acme: Add support for Oracle ACME session border controller.
  * Add debian/upstream/metadata.
  * Add lintian-overrides for perl in sh script.
  * Install (disabled) systemd timers as cron.d alternative.

 -- Roland Rosenfeld <roland@debian.org>  Tue, 04 Oct 2022 13:51:50 +0200

rancid (3.13-1) unstable; urgency=medium

  * New upstream version 3.13.
  * Update all patches to new upstream version (11_man_typo,
    16_juniper_temperature, 17_export_LIST_OF_GROUPS, 27_spelling,
    31_intro_man7, 32_login_man are now incorporated upstream).
  * Support rancid_intro(7) and lg_intro(7) in debian/rules.
  * Optimize debian/watch file.
  * salsa-ci.yml: Remove diffoscope.
  * Update debhelper to v13.
  * Optimize some DEP-3 patch headers.

 -- Roland Rosenfeld <roland@debian.org>  Thu, 15 Oct 2020 17:26:17 +0200

rancid (3.12-1) unstable; urgency=medium

  * New upstream version 3.12.

 -- Roland Rosenfeld <roland@debian.org>  Tue, 10 Mar 2020 18:17:45 +0100

rancid (3.11-1) unstable; urgency=medium

  * New upstream version 3.11.
  * Update all patches to new upstream version.
  * Get rid of rancid-cgi.preinst, which contained only ancient fixup
    (Closes: #942388).
  * gitlab-ci: reprotest enable diffoscope.
  * debian/watch: switch from ftp to https.
  * Symlink some binaries to /usr/bin to make lintian happy.
  * Move rancid_intro(1) and lg_intro(1) to man section 7.
  * 32_login_man: Add some documentation for a10login, brlogin, fxlogin,
    rblogin.
  * Update to Standards-Version 4.5.0 (no changes).
  * Rename gitlab-ci.yml to salsa-ci.yml.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 01 Feb 2020 17:12:11 +0100

rancid (3.10-1) unstable; urgency=medium

  * New upstream release 3.10.
  * Adapt all patches to new version.
  * d/gitlab-ci.yml stripped down using pipline-jobs.yml
  * Define version test as superficial.
  * Build-depend on debhelper-compat (= 12) instead of using d/compat.
  * Upgrade to Standards-Version 4.4.1 (no changes).

 -- Roland Rosenfeld <roland@debian.org>  Thu, 03 Oct 2019 09:43:52 +0200

rancid (3.9-1) unstable; urgency=medium

  * New upstream version 3.9.
  * Update all patches to new release.
  * Remove 28_smc-m8024k.patch (already incorporated upstream).
  * Add buildlog to gitlab-ci test pipeline.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 26 Jan 2019 12:24:41 +0100

rancid (3.8-5) unstable; urgency=medium

  * Upgrade debhelper to v12.
  * Use secure URI in Homepage field.
  * Drop unnecessary dependency on dh-autoconf.
  * Add minimal autotest suite, testing rancid_par and version output of
    all programs.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 11 Jan 2019 15:50:19 +0100

rancid (3.8-4) unstable; urgency=medium

  * 29_reproducible_ENV_PATH: Hard code ENV_PATH in config files instead of
    generating it via autoconf to make package reproducible (Closes: #915847).
  * 30_autoconf_ping: Avoid running ping in autoconf but hardcode
    LG_PING_CMD in debian/rules to make package reproducible on salsa.

 -- Roland Rosenfeld <roland@debian.org>  Tue, 01 Jan 2019 16:16:27 +0100

rancid (3.8-3) unstable; urgency=medium

  * Add salsa CI pipeline in debian/gitlab-ci.yml.
  * Explicitly pass ping path to configure to fix reproducible build on
    merged-usr vs non-merged systems.  Thanks to Andreas Henriksson.
    (Closes: #915847)
  * Upgrade to Standards-Version 4.3.0 (no changes).

 -- Roland Rosenfeld <roland@debian.org>  Wed, 26 Dec 2018 12:10:44 +0100

rancid (3.8-2) unstable; urgency=medium

  * Upgrade to Standards-Version 4.2.1 (no changes).
  * 28_smc-m8024k: srancid: prevent ShowSys() power supply handling from
    consuming too much (Closes: #907035)

 -- Roland Rosenfeld <roland@debian.org>  Mon, 27 Aug 2018 18:41:48 +0200

rancid (3.8-1) unstable; urgency=medium

  * New upstream version 3.8.
  * Update debian/copyright.
  * Adapt all patches to new version.
  * Check upstream signature in debian/watch.
  * Upgrade Standards-Version to 4.1.5.
    - Declare Rules-Requires-Root: no.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 21 Jul 2018 16:45:48 +0200

rancid (3.7-2) unstable; urgency=medium

  * Changed Vcs to salsa.debian.org.
  * Upgrade Standards-Version to 4.1.4 (use https in copyright).
  * Upgrade debhelper to v11.
  * Build-Depend on default-mta instead of exim4.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 29 Apr 2018 12:20:00 +0200

rancid (3.7-1) unstable; urgency=medium

  * New upstream version 3.7.
  * Adapt all patches to new version.
  * Update debian/copyright.
  * Remove empty postrm.
  * 27_spelling: Fix spelling error in man page.
  * Upgrade Standards-Version to 4.1.1 (no changes).
  * Upgrade debhelper to v10 and remove autotools-dev.
  * Remove dh-autoreconf build dependeny, since this is already in dh v10.

 -- Roland Rosenfeld <roland@debian.org>  Tue, 31 Oct 2017 17:14:13 +0100

rancid (3.6.2-2) unstable; urgency=medium

  * Fix handling of /etc/rancid/rancid.types.conf, which was formerly
    created (empty) in postinst and is now a conffile (Closes: #851582).

 -- Roland Rosenfeld <roland@debian.org>  Mon, 16 Jan 2017 17:57:59 +0100

rancid (3.6.2-1) unstable; urgency=medium

  * New upstream maintenance release 3.6.2.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 14 Jan 2017 13:41:42 +0100

rancid (3.6.1-1) unstable; urgency=medium

  * New upstream maintenance release 3.6.1.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 17 Dec 2016 16:46:26 +0100

rancid (3.6.0-1) unstable; urgency=medium

  * New upstream version 3.6.0.
  * Adapt all patches to new version.

 -- Roland Rosenfeld <roland@debian.org>  Tue, 06 Dec 2016 08:42:55 +0100

rancid (3.5.1-1) unstable; urgency=medium

  * New upstream version 3.5.1.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 09 Sep 2016 10:38:38 +0200

rancid (3.5.0-1) unstable; urgency=medium

  * New upstream version 3.5.0.
  * 35_3des_cbc: no longer needed, since upstream uses the SSH default now
    (see FAQ S3 for more info about this).
  * 36_mrvlogin_disable_paging and 37_mrv_noise are now incorporated upstream.
  * Adapted all other patches to new upstream version.
  * 11_man_typo: fix another typo.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 27 Aug 2016 23:30:32 +0200

rancid (3.4.1-5) unstable; urgency=medium

  * Remove 08_hlogin_paging, since this seems to be broken and upstream
    implemented some major updates in 3.3.0 on hlogin (Closes: #824153).

 -- Roland Rosenfeld <roland@debian.org>  Fri, 13 May 2016 07:50:13 +0200

rancid (3.4.1-4) unstable; urgency=medium

  * 36_mrvlogin_disable_paging: Disable paging of MRV optiswitch devices.
  * 37_mrv_noise: Filter more noise from MRV show version.
  * Thanks to Christian Rohmann for providing these two patches.
  * Upgrade Standards-Version to 3.9.8 (no changes).
  * Remove host and build types from debian/rules.
  * Enable more hardening (pie and bindnow).

 -- Roland Rosenfeld <roland@debian.org>  Mon, 09 May 2016 16:56:34 +0200

rancid (3.4.1-3) unstable; urgency=medium

  * Damn, I missed hurd with the previous change.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 12 Mar 2016 17:37:18 +0100

rancid (3.4.1-2) unstable; urgency=medium

  * Build-Depends on inetutils-ping [kfreebsd-any], since iputils-ping
    doesn't exist on BSD (Closes: #817983).

 -- Roland Rosenfeld <roland@debian.org>  Sat, 12 Mar 2016 16:12:01 +0100

rancid (3.4.1-1) unstable; urgency=medium

  * New upstream version 3.4.1.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 19 Feb 2016 12:01:06 +0100

rancid (3.4.0-1) unstable; urgency=medium

  * New upstream version 3.4.0.
  * Adapt all patches to new upstream version.
  * 24_mrv_temperature is now incorporated upstream.
  * Update de.po, it.po and sv.po to UTF-8.
  * Run debconf-updatepo.
  * Add -p to QUILT_DIFF_OPTS.

 -- Roland Rosenfeld <roland@debian.org>  Tue, 16 Feb 2016 08:27:24 +0100

rancid (3.3.0-1) unstable; urgency=medium

  * New upstream version 3.3.0.
  * Adapt all patches to new upstream version.
  * Remove rancid-3.2.p[1-7] and 34_alogin, which are incorporated upstream.
  * Upgrade Standards-Version to 3.9.7 (no changes).

 -- Roland Rosenfeld <roland@debian.org>  Thu, 04 Feb 2016 19:29:50 +0100

rancid (3.2-4) unstable; urgency=medium

  * Fix Vcs-Git URL from last change.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 31 Jan 2016 11:15:47 +0100

rancid (3.2-3) unstable; urgency=medium

  * Remove perl<<5.12.3-7 as alternative to libperl4-corelibs-perl.
  * Depend and Build-Depend on iputils-perl, since this is available on
    architectures now.
  * Run wrap-and-sort.
  * Change default SSH cipher from 3des to 3des-cbc, since openssh-client 7
    does not longer support cipher 3des (Closes: #809190).
  * Add pt_BR.po, thanks to Adriano Rafael Gomes (Closes: #811527).
  * Change VCS URLs to encrypted protocol https.
  * 11_man_typo: Fix another typo in rancid.conf(5) man page.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 30 Jan 2016 09:47:13 +0100

rancid (3.2-2) unstable; urgency=medium

  * rancid-cgi: stop creating logfile in postinst (Closes: #784537).
  * Fix NEWS vs. changelog urgency mismatch.
  * Stop installing NEWS file to rancid-cgi package.
  * Create empty rancid.types.conf in postinst and remove it on purge
    (Closes: #794715).
  * 27_rancid-3.2.p1: rancid.pm: check result from inet_pton().
  * 28_rancid-3.2.p2: junos.pm: fix showsystemlicense for errant licenses
    w/ 0 available.
  * 29_rancid-3.2.p3: configure, rancid.pm: check minimum revision of
    Socket.pm.
  * 30_rancid-3.2.p4: rancid.type.base: comment adtran definition to avoid
    confusion.
  * 31_rancid-3.2.p5: panos.pm: fix package definition.
  * 32_rancid-3.2.p6: control_rancid: fix handling of configs/.cvsignore.
  * 33_rancid-3.2.p7: control_rancid: fix handling of <group>/rancid.conf
    in git - Mike Eklund.
  * 34_alogin: Avoid crashing alogin (patch from upstream alpha 3.2.99)
    (Closes: #794714).

 -- Roland Rosenfeld <roland@debian.org>  Thu, 20 Aug 2015 15:47:25 +0200

rancid (3.2-1) unstable; urgency=medium

  * Uploaded to unstable since there where no big bugs found.
  * New upstream version 3.2 (Closes: #780786).
  * Adapt all patches to new version.
  * This includes Git support (Closes: #731744) (mention it in control).
  * Remove duplicates from rancid.docs.
  * Update rancid-cgi description.
  * Several changes on rancid-cgi, initially developed by Jesse Norell
    <jesse@kci.net>:
    - Add a separate README.Debian to rancid-cgi.
    - Add a better apache2 configuration.
    - Add logrotation.
  * Move index.html and lgnotes.html to /usr/share/rancid-cgi/
  * set -e in rancid.config instead of passing in #!

 -- Roland Rosenfeld <roland@debian.org>  Sun, 26 Apr 2015 15:24:54 +0200

rancid (3.1-2) experimental; urgency=medium

  * Update debian/copyright.
  * 24_mrv_temperature: Remove lines from mrv configuration that are
    changed because of timestamps every day.
  * Now really install NEWS.Debian, to tell users that router.db separator
    was changed from ':' to ';'.  (Closes: #778623)

 -- Roland Rosenfeld <roland@debian.org>  Tue, 17 Feb 2015 17:07:51 +0100

rancid (3.1-1) experimental; urgency=low

  * New upstream version 3.1 (Closes: #751975).
  * Adapt all patches to new version.
  * The following patches are already incorporated upstream:
    12_MAILPLUS, 14_rancid-2.3.8.p1, 15_rancid-2.3.8.p2, 18_AM_C_PROTOTYPES,
    19_rancid-2.3.8.p3, 20_rancid-2.3.8.p4, 21_arista_free_flash
  * Add --with-autotools_dev --with-autoreconf to debian/rules.
  * Add autotools-dev to Build-Depends.
  * 23_configure.vers_path: Call configure.vers with local dir path.
  * Use canonical URI in VCS fields.
  * Upgrade Standards-Version to 3.9.6 (no changes).
  * Add username rancid to cron.d, since cron.d needs this.
  * Install perl modules to /usr/share/perl5/rancid.
  * Add NEWS.Debian to mention that separator in router.db was changed from
    ':' to ';', which user has to do manually on upgrade.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 31 Jan 2015 20:36:06 +0100

rancid (2.3.8-7) unstable; urgency=low

  * 22_juniper_timestamps: Remove lines from juniper configuration that
    are changed because of timestamps every day. Thanks to Christian
    Hammers.

 -- Roland Rosenfeld <roland@debian.org>  Sat, 30 Nov 2013 22:40:56 +0100

rancid (2.3.8-6) unstable; urgency=low

  * automake --add-missing to build on current sid (Closes: #724338).

 -- Roland Rosenfeld <roland@debian.org>  Mon, 30 Sep 2013 08:32:51 +0200

rancid (2.3.8-5) unstable; urgency=low

  * 18_AM_C_PROTOTYPES: Remove AC_C_PROTOTYPES from configure.in since
    this is no longer supported by current aclocal and breaks autobuild
    (Closes: #713585).
  * 19_rancid-2.3.8.p3: fnlogin: bugfix for port setting.
  * 20_rancid-2.3.8.p4: fnrancid: filter cycling RSA private keys.
  * Upgrade Standards-Version to 3.9.4 (no changes).
  * Install /etc/cron.d/rancid with commented jobs instead of installing
    this to doc/examples (Closes: #505472)
  * 21_arista_free_flash: Round Free flash remaining on Arista
    (Closes: #706411)

 -- Roland Rosenfeld <roland@debian.org>  Wed, 03 Jul 2013 18:32:47 +0200

rancid (2.3.8-4) unstable; urgency=low

  * 16_juniper_temperature:  Remove the temperature values from juniper
    configuration, they just create nonsense updates.  Thanks to Christian
    Rohmann for providing a patch.
  * 17_export_LIST_OF_GROUPS: export LIST_OF_GROUPS variable in
    rancid.conf (Closes: #699011)

 -- Roland Rosenfeld <roland@debian.org>  Tue, 29 Jan 2013 17:32:20 +0100

rancid (2.3.8-3) unstable; urgency=low

  * Force run aclocal, autoconf, autoheader, automake in debian/rules,
    otherwise the files are not recreated via buildd.
    This also Closes: #675000.
  * Remove the bin/Makefile.in part from 13_CFLAGS.patch, because this is
    automatically changed by the above.

 -- Roland Rosenfeld <roland@debian.org>  Tue, 29 May 2012 18:28:30 +0200

rancid (2.3.8-2) unstable; urgency=low

  * Add Vcs-* to control, because this package should be git maintained in
    the future.
  * Build-Depend on inetutils-ping [!s390x] | iputils-ping [s390x] | ping
    to work around problems with s390x build daemon.
  * Update Standards-Version to 3.9.2 (no changes).
  * Change debian/copyright to copyright-format 1.0.
  * Change debian/compat to 9 and build-depend on debhelper >=9 to enable
    hardening.
  * Remove OPTIMIZE from debian/rules, because this is automatically
    handled by debhelper.
  * 13_CFLAGS: remove CFLAGS from bin/Makefile.{in|am} to let debhelper
    set these for hardening options.
  * Add ${perl:Depends} to dependencies.
  * 14_rancid-2.3.8.p1: hpuifilter: don't use memcpy for overlapping regions.
  * 15_rancid-2.3.8.p2: xrrancid: additional file filters for 4.2.

 -- Roland Rosenfeld <roland@debian.org>  Fri, 18 May 2012 20:13:42 +0200

rancid (2.3.8-1) unstable; urgency=low

  * New upstream version 2.3.8 (Closes: #660390)
    Thanks to Marc Haber for providing an initial merged version.
  * Upgrade all patches to new version. (09_svn is no longer needed)
  * Add automake to build-depends (Closes: #660374)
  * Add Marc Haber to Uploaders.
  * Add nl debconf translation. Thanks to Jeroen Schot (Closes: #661584).
  * Disable override_dh_auto_test, which causes build trouble on sid.

 -- Roland Rosenfeld <roland@debian.org>  Thu, 12 Apr 2012 23:27:51 +0200

rancid (2.3.6-2) unstable; urgency=low

  * Build-Depend on inetutils-ping instead of iputils-ping to build on
    kfreebsd.
  * Create user rancid with /bin/bash shell (Closes: #654353).
  * Change user rancids shell from /bin/false to /bin/bash in postinst.
  * Migrate from dpatch to 3.0 (quilt) format.
  * Reformat all patches.
  * Change build depenency from dpatch to debhelper (>= 7.0.50~).
  * Change debian/compat to "7".
  * Remove README.source.
  * Complete rewrite of debian/rules.
  * Depend on  libperl4-corelibs-perl | perl (<< 5.12.3-7), because rtrfilter
    still uses newgetopt.pl.
  * Update Standards-Version to 3.9.2 (no changes).
  * Remove transitional packages rancid-core and rancid-util.
  * 12_MAILPLUS.patch: Replace @MAILPLUS@ and @ADMINMAILPLUS@ in downreport.
    Thanks to GeekSmith for reporting and providing a patch.  This fixes
    Ubuntu bug #909877.

 -- Roland Rosenfeld <roland@debian.org>  Tue, 03 Jan 2012 17:47:50 +0100

rancid (2.3.6-1) unstable; urgency=low

  * New upstream release 2.3.6 (Closes: #612366).
    - This already fixes the CatOS SNMP community issue (Closes: #579195).
    - This already fixes the CatOS localuser password issue (Closes: #578842).
  * Adapt all patches to new version.
  * Remove 10_iosxr, it's now included upstream.
  * Update da.po. Thanks to Joe Dalton <joedalton2@yahoo.dk> (Closes: #598783).
  * Add debian/source/format 1.0.
  * Update to Standards-Version 3.9.1
    - Change versioned Conflicts to Breaks.
  * 08_hlogin_paging: Gracefully handle errors on ProCurve switches that
    don't support "terminal length 0".  Thanks to Matej Vela
    <vela@debian.org> (Closes: #577111)

 -- Roland Rosenfeld <roland@debian.org>  Wed, 02 Mar 2011 23:45:25 +0100

rancid (2.3.3-1) unstable; urgency=low

  * New upstream release 2.3.3.
  * Adapt all patches to the new version.
  * 09_svn: Remove control_rancid.in.orig from patch (Closes: #555548).
  * 10_iosxr: Add proper support for Cisco IOS XR devices. Thanks to Per
    Carlson (Closes: #556462).
  * Change rancid-cgi package description to be more informative.
    (Closes: #493629).
  * Added debian/README.source (from dpatch package) to explain how dpatch
    works.
  * Update to Standards-Version 3.8.4 (no changes).
  * 11_man_typo: Fix some typos from the man pages.

 -- Roland Rosenfeld <roland@debian.org>  Mon, 17 May 2010 21:17:57 +0200

rancid (2.3.2-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix pending l10n issues. Debconf translations:
    - Russian (Yuri Kozlov).  Closes: #546342
    - Japanese (Hideki Yamane (Debian-JP)).  Closes: #558059
    - Italian (Samuele Giovanni Tonon).
    - Spanish (Francisco Javier Cuadrado).  Closes: #574091
    - Czech (Miroslav Kure).  Closes: #574162
    - Vietnamese (Clytie Siddall).  Closes: #574434

 -- Christian Perrier <bubulle@debian.org>  Fri, 19 Mar 2010 06:53:10 +0100

rancid (2.3.2-1) unstable; urgency=low

  * New upstream release 2.3.2.
  * Merge rancid-core and rancid-util into a new package rancid.
  * Adapt all patches to the new version.
  * 07_nrancid_bs_paging: Fix "--- more ---" & backspace style paging on
    netscreen devices.  Thanks to Vaclav Ovsik <vaclav.ovsik@i.cz> for
    providing a patch (Closes: #520899).
  * 08_hlogin_paging: Turn off paging on HP equipment.  Thanks to James
    W. Laferriere <babydr@baby-dragons.com> for providing a patch.
  * 09_svn: Avoid "svn: Directory '<GROUP>' is out of date" message.
    Thanks to Nicolas DEFFAYET <nicolas-ml@deffayet.com> for providing a
    patch.
  * Update to Standards-Version 3.8.1 (no changes).

 -- Roland Rosenfeld <roland@debian.org>  Thu, 11 Jun 2009 14:24:49 +0200

rancid (2.3.2~a8-4) unstable; urgency=medium

  * rancid-core.postinst: Only try to add the rancid user when he does not
    exist. Thanks to Evgeni Golov for providing a patch (Closes: #493988)

 -- Roland Rosenfeld <roland@debian.org>  Sat, 15 Nov 2008 12:24:53 +0100

rancid (2.3.2~a8-3) unstable; urgency=low

  * 06_tmp_security.dpatch: remove temporary directory recursively.  This
    bug was introduced with the previous security fix
    (Closes: #497972, #500025).

 -- Roland Rosenfeld <roland@debian.org>  Wed, 24 Sep 2008 20:56:11 +0200

rancid (2.3.2~a8-2) unstable; urgency=high

  * 06_tmp_security.dpatch: Fix some temp file security vulnerabilities by
    using mktemp(1) or moving the lockfiles or tempfiles to
    /var/lib/rancid, where they should be safe (Closes: #496426).
  * Remove "XS-Autobuild: yes" from debian/control, since we are in main now.

 -- Roland Rosenfeld <roland@debian.org>  Sun, 24 Aug 2008 23:29:23 +0200

rancid (2.3.2~a8-1) unstable; urgency=low

  * New upstream alpha version 2.3.2a8.
  * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!! This version uses a new copyright which is BSD compatible, !!!
    !!! so the package can now be moved into main!                 !!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  * Upgrade all patches to the new upstream version.
  * Upgrade sv.po.  Thanks to Martin Bagge (Closes: #488181).
  * Add ${shlibs:Depends}, ${misc:Depends} to rancid-core dependencies.
  * 05_man_fixup: Comment out unknown commands .El, .Bl, and .ID in man
    pages to make lintian happy.
  * Add debian/watch file.
  * Update to Standards-Version 3.8.0 (no changes).

 -- Roland Rosenfeld <roland@debian.org>  Sun, 29 Jun 2008 12:51:07 +0200

rancid (2.3.2~a7-3) unstable; urgency=low

  * Add "XS-Autobuild: yes" to control and justification to copyright to
    allow autobuilding of this package.
  * Move Homepage from package description to Source header.
  * Update to Standards-Version 3.7.3 (no changes).

 -- Roland Rosenfeld <roland@debian.org>  Sat, 12 Jan 2008 19:17:33 +0100

rancid (2.3.2~a7-2) unstable; urgency=low

  * Update de.po.  Thanks to Helge Kreutzmann (Closes: #439632).
  * Update pt.po.  Thanks to Rui Branco (Closes: #439692).
  * Update fr.po.  Thanks to Christian Perrier (Closes: #440393).

 -- Roland Rosenfeld <roland@debian.org>  Sat,  8 Sep 2007 13:58:40 +0200

rancid (2.3.2~a7-1) unstable; urgency=low

  * New Debian maintainer (Closes: #424722).
  * New upstream (alpha) version 2.3.2a7 (Closes: #342274).
  * Re-added lost changelog entries (2.3.0-3 and 2.3.1-1).
  * Upgrade to debhelper 5 (no real changes).
  * Remove config.[sub|guess] from debian patch, because they are never
    used.
  * Fix minor typos in debconf templates.  Thanks to Helge Kreutzmann
    for providing the fixes (Closes: #412321).
  * Add German debconf translaten.  Thanks to Helge Kreutzmann
    (Closes: #412323).
  * Build the package for all architectures again (hopefully the
    experimental buildd network does support this). This removes
    rancid-installer package.
  * Make rancid-utils and rancid-cgi "Architecture: all" instead of "any".
  * Add Homepage to package descriptions.
  * 01_rancid_par: Really rename par to rancid_par now.
  * Removed empty rancid-cgi.postinst, rancid-util.postinst,
    rancid-{core|util|cgi}.prerm.
  * Remove debconf stuff from rancid-cgi.preinst and rancid-core.preinst,
    because debconf isn't used there.
  * rancid-core.postinst: Fix non POSIX stuff.
  * Change PREFIX in debian/rules from /usr to / and all other paths
    accordingly to avoid /usr/../var and the like in the scripts.
  * Remove dependency on csh|tcsh, cause this isn't needed.
  * Create user rancid using adduser instead of useradd/usermod.
  * Change debconf question "Let's go!" to "Really continue?" to make it a
    question and remove question from extended description to make lintian
    happy.
  * Update to Standards-Version 3.7.2 (no changes).
  * 02_man_hyphen: Fix hyphens used as minus signs in several man pages.
  * 03_diffstat: add diffstat output to changes e-mail (Closes: #357218).
  * Depends on "openssh-client | ssh" instead of "ssh" now. Build-Depends
    on openssh-client instead of ssh now.
  * rancid-cgi depends on "apache2 | httpd-cgi" instead of "apache | httpd"
    now.
  * Depends on "cvs | subversion" now, because 2.3.2 supports svn as a cvs
    alternative.
  * 04_pixlogin.dpatch by Emre Bastuz <info@emre.de>:
    Allow additional option "add login" in .cloginrc to allow login
    to Cisco PIX via SSH instead of using enable.  For more info see:
    http://www.shrubbery.net/pipermail/rancid-discuss/2005-August/001159.html
    or /usr/share/doc/rancid-core/README.Debian
  * Depend/Build-Depend on "iputils-ping | ping", because this should also
    work with other variants of ping.
  * Build-Depend on "exim4 | mail-transport-agent", otherwise /usr/sbin is
    missing from the PATH in rancid.conf and /usr/sbin/sendmail cannot be
    found.
  * Build-Depend on "telnet", otherwise the PATH contains ".", which is
    insecure.
  * Add Portuguese debconf translaten.  Thanks to Miguel Figueiredo
    (Closes: #437390).

 -- Roland Rosenfeld <roland@debian.org>  Sat, 18 Aug 2007 11:14:18 +0200

rancid (2.3.1-4) unstable; urgency=low

  * Added Depends to debconf-2.0 (Closes: #332081)
  * Closing bug about Swedish translation (Closes: #330456)

 -- Samuele Giovanni Tonon <samu@debian.org>  Sat, 29 Oct 2005 11:18:10 +0200

rancid (2.3.1-3) unstable; urgency=low

  * Added Swedish translation, thanks to Daniel Nylander

 -- Samuele Giovanni Tonon <samu@debian.org>  Wed, 28 Sep 2005 09:34:00 +0200

rancid (2.3.1-2) unstable; urgency=low

  * Added amd64 to arch that can be compiled (Closes: #306311)
  * Added Czech translation, thanks to Miroslav Kure
    (Closes: #319636)

 -- Samuele Giovanni Tonon <samu@debian.org>  Thu, 11 Aug 2005 12:04:35 +0200

rancid (2.3.1-1) unstable; urgency=low

  * Initial Release.
  * Added Danish translation, thanks to Morten Brix Pedersen (Closes: #253027)
  * Added fixed nrancid and nlogin, thanks to Sebastien Barbereau
    (Closes: #250254)
  * Added a note on Readme.Debian to help ppl with problem with session
    hanging

 -- Samuele Giovanni Tonon <samu@debian.org>  Fri, 11 Jun 2004 11:35:51 +0200

rancid (2.3.0-3) unstable; urgency=low

  * Added liblockfile-simple-perl to rancid-cgi depends, thanks to
    Ernesto Hernandez-Novich for reporting (Closes: #247752)

 -- Samuele Giovanni Tonon <samu@debian.org>  Thu,  6 May 2004 23:44:52 +0200

rancid (2.3.0-2) unstable; urgency=low

  * Added French templates translation, thanks to Christian Perrier
    (Closes:#243642)

 -- Samuele Giovanni Tonon <samu@debian.org>  Thu, 15 Apr 2004 23:47:41 +0200

rancid (2.3.0-1) unstable; urgency=low

  * New upstream release
  * Upstream Author fixed man page for control_rancid (Closes:#219908)
  * control_rancid should not source env because it's called from do-diff
    as the man page say (Closes:#219917)
  * renamed apache.conf in rancid-lg.conf

 -- Samuele Giovanni Tonon <samu@debian.org>  Sun, 28 Mar 2004 15:33:33 +0200

rancid (2.2.2-13) unstable; urgency=low

  * Added dpatch to use patch available on rancid site (Closes:#225524)

 -- Samuele Giovanni Tonon <samu@debian.org>  Tue, 30 Dec 2003 19:03:17 +0100

rancid (2.2.2-12) unstable; urgency=low

  * Fixed copyright file

 -- Samuele Giovanni Tonon <samu@debian.org>  Thu, 18 Dec 2003 12:27:54 +0100

rancid (2.2.2-11) unstable; urgency=low

  * Moved rancid-installer to non-free/net

 -- Samuele Giovanni Tonon <samu@debian.org>  Sun,  2 Feb 2003 11:16:45 +0100

rancid (2.2.2-10) unstable; urgency=low

  * Fixed Build Depends, netkit-ping instead of ping alone (Closes: #178975)
  * Added some hacks to make rancid-installer build for every architecture
  * Added description on how to compile in debian/README.rancid-installer

 -- Samuele Giovanni Tonon <samu@debian.org>  Thu, 30 Jan 2003 19:06:38 +0100

rancid (2.2.2-9) unstable; urgency=low

  * Fixed Build Depends (Closes: #173354)

 -- Samuele Giovanni Tonon <samu@debian.org>  Fri, 24 Jan 2003 18:46:53 +0100

rancid (2.2.2-8) unstable; urgency=low

  * Added ping, cvs and ssh to build depends Section
  * Added ping and ssh to Depends Section

 -- Samuele Giovanni Tonon <samu@debian.org>  Mon, 16 Dec 2002 22:47:27 +0100

rancid (2.2.2-7) unstable; urgency=low

  * Added some information of what rancid is in rancid-installer
    description (Closes: Bug#173106)

 -- Samuele Giovanni Tonon <samu@debian.org>  Mon, 16 Dec 2002 18:30:38 +0100

rancid (2.2.2-6) unstable; urgency=low

  * fixed paths on cron.example
  * moved from /usr/lib/rancid to /var/lib/rancid (to best fit things
    following fhs)
  * added link from /var/log/rancid to /var/lib/rancid/logs
  * added a control for default home of user rancid
  * added a lots of symbolic links to well fit the original structure

 -- Samuele Giovanni Tonon <samu@debian.org>  Wed, 13 Nov 2002 18:58:00 +0100

rancid (2.2.2-5) unstable; urgency=low

  * Changed in control_rancid par and rename in to rancid_par and
    rancid_rename, thanks to Wolfgang Tremmel for reporting it
    (Closes: Bug#163074)

 -- Samuele Giovanni Tonon <samu@debian.org>  Fri, 11 Oct 2002 11:49:51 +0200

rancid (2.2.2-4) unstable; urgency=low

  * removed dependencies from libc6

 -- Samuele Giovanni Tonon <samu@debian.org>  Sun,  6 Oct 2002 18:33:35 +0200

rancid (2.2.2-3) unstable; urgency=low

  * Moved apache.conf from /etc/rancid into doc/example
  * lg.conf it's now in /etc/rancid with symlink to
    /usr/lib/rancid/util/lg/lg.conf
  * changed bin dir, now all it's in /usr/bin
  * env is a conffile in /etc/rancid
  * Renamed par in rancid_par as well as his manpage (Closes: Bug#163074)
  * Renamed rename in rancid_rename
  * Added rancid_rename man page (copied from perl rename man page)
  * Modified all refer to bin/env to /etc/rancid/env

 -- Samuele Giovanni Tonon <samu@debian.org>  Sun,  6 Oct 2002 17:19:00 +0200

rancid (2.2.2-2) unstable; urgency=low

  * unstable release (Closes: Bug#125894)

 -- Samuele Giovanni Tonon <samu@debian.org>  Thu, 26 Sep 2002 02:30:46 +0200

rancid (2.2.2-1) experimental; urgency=low

  * Initial Release.
  * Modified Makefile.am, Makefile.in, util/Makefile.in util/lg/Makefile.in
    to follow FHS
  * added /etc/rancid/apache.conf to add to your apache configuration to
    make cgi visible  under http://wbserver/rancid/
  * added a cron and a .cshrc example in /usr/share/doc/rancid-core/examples/

 -- Samuele Giovanni Tonon <samu@debian.org>  Thu, 12 Sep 2002 18:28:39 +0200
