#!/usr/bin/make -f
#
# (c) 2007-2023  Roland Rosenfeld <roland@debian.org>
#
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
#export DH_OPTIONS=-v

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

%:
	dh $@

override_dh_autoreconf:
#	preserver files deleted by distclean
	tar cf debian/preserve.tar Makefile.in aclocal.m4 bin/Makefile.in \
		configure etc/Makefile.in include/Makefile.in \
		include/config.h include/version.h install-sh \
		lib/Makefile.in man/Makefile.in mkinstalldirs \
		share/Makefile.in
	dh_autoreconf

override_dh_auto_configure:
	dh_auto_configure -- \
		PING_PATH=/bin/ping \
		LG_PING_CMD="/bin/ping -c 1" \
		--prefix=/ \
		--mandir=\$${prefix}usr/share/man \
		--infodir=\$${prefix}usr/share/info \
		--datadir=\$${prefix}var/lib \
		--sysconfdir=\$${prefix}etc/rancid \
		--exec-prefix=\$${prefix}usr/lib/rancid \
		--localstatedir=\$${prefix}var/lib/rancid \
		--libdir=\$${prefix}usr/share/perl5

override_dh_auto_install:
	$(MAKE) install prefix=$(CURDIR)/debian/rancid/ \
		pkgdata_DATA='' dist_pkgdata_DATA=''

#	rename par to rancid_par to avoid conflicts with par:
	(cd debian/rancid/usr/share/man/man1; mv par.1 rancid_par.1)
	(cd debian/rancid/usr/lib/rancid/bin; mv par rancid_par)

#	symlink some binaries from /usr/lib/rancid/bin to /usr/bin
	(cd debian/rancid/usr/bin; \
	 for f in ../lib/rancid/bin/*rancid ../lib/rancid/bin/*login \
		../lib/rancid/bin/rancid-cvs ../lib/rancid/bin/rancid_par ; \
	 do \
		ln -s $$f; \
	 done)

#	rancid-cgi:
	if [ -d debian/rancid-cgi/ ]; then \
	   mv debian/rancid/usr/lib/rancid/bin/lg.cgi \
	      debian/rancid-cgi/usr/lib/cgi-bin/lg/; \
	   mv debian/rancid/usr/lib/rancid/bin/lgform.cgi \
	      debian/rancid-cgi/usr/lib/cgi-bin/lg/; \
	   mv debian/rancid/etc/rancid/lg.conf \
	      debian/rancid-cgi/etc/rancid/; \
	   mv debian/rancid/usr/share/man/man7/lg_intro.7 \
	      debian/rancid-cgi/usr/share/man/man7/lg_intro.7; \
	   mv debian/rancid/usr/share/man/man5/lg.conf.5 \
	      debian/rancid-cgi/usr/share/man/man5/; \
	   install -m644 share/index.html \
	      debian/rancid-cgi/usr/share/rancid-cgi/; \
	   install -m644 share/lgnotes.html \
	      debian/rancid-cgi/usr/share/rancid-cgi/; \
	else \
	   rm -f debian/rancid/usr/lib/rancid/bin/lg.cgi \
		 debian/rancid/usr/lib/rancid/bin/lgform.cgi \
		 debian/rancid/etc/rancid/lg.conf \
		 debian/rancid/usr/share/man/man7/lg_intro.7 \
		 debian/rancid/usr/share/man/man5/lg.conf.5; \
	fi

override_dh_clean:
	rm -f config.log bin/control_rancid bin/hrancid
	[ ! -f Makefile ] || $(MAKE) distclean
	dh_clean

#	restore files deleted by distclean
	[ ! -f debian/preserve.tar ] || tar xf debian/preserve.tar
	rm -f debian/preserve.tar

override_dh_installchangelogs:
	dh_installchangelogs -k CHANGES

override_dh_compress:
	dh_compress -X.pdf

override_dh_auto_test:
# do nothing, because this fails with sid...

override_dh_installsystemd:
	dh_installsystemd --no-enable --name rancid
	dh_installsystemd --no-enable --name rancid-cleanup
